package view;

import java.util.Scanner;

import service.Login;

public class AccountMenu {
	public void accountMenu() {
		Scanner scan = new Scanner(System.in);
		int menu = 0;
		Login login = new Login();
		MenuView menuView = new MenuView ();
		ActionsMenu actionsMenu = new ActionsMenu();
		while (menu != 4) {
			System.out.println("Choose option from menu: \n" + "  1 - Account \n" + "  2 - logout \n");
			menu = scan.nextInt();
			switch (menu) {
			case 1:
				actionsMenu.operationInAccountMenu();
				break;
			case 2:
				boolean resultLogOut = login.logout();
				if (resultLogOut) {
					menuView.mainMenu();
				}
				break;

			default:
				System.out.println("Error choice not on menu. \n");
			}
		}
	}

}
