package view;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.User;
import entity.Person;
import service.Login;

public class MenuView {
	
	public static void mainMenu() {
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(User.class)
				.addAnnotatedClass(Person.class)
				.buildSessionFactory();

				
		//create session
		Session session = factory.getCurrentSession();

		try {


			//start a transaction
			session.beginTransaction();

			//query students
			List<User> theUsers = session.createQuery("from User").getResultList();
			Login instanceLogin = new Login();
			AccountMenu accountMenu = new AccountMenu();
			Scanner scan = new Scanner(System.in);
			int menu = 0;

			while (menu != 3) {
				System.out.println("Please choose option from menu: \n" + "  1 - Login. \n" + "  2 - Exit \n");
				menu = scan.nextInt();
				switch (menu) {
				case 1:
					boolean resultLogin = instanceLogin.login(theUsers);
					if (resultLogin) {
						accountMenu.accountMenu();
					}
					break;

				case 2:
					System.exit(0);

				default:
					System.out.println("Error choice not on menu. \n");
					mainMenu();
				}
			}



			//commit transaction
			session.getTransaction().commit();

			System.out.println("Done!!!Uraaaa");
		}
		finally {
			factory.close();
		}


	}


	}

