package view;

import java.util.Scanner;

public class ActionsMenu {
	public void operationInAccountMenu() {
		Scanner scan = new Scanner(System.in);
		AccountMenu accountMenu = new AccountMenu();
		int menu = 0;

		while (menu != 4) {
			System.out.println("Choose option from menu: \n" + "  1 - CreateAccount \n"
					+ "  2 - Display accounts for user-name \n" + "  3 - Make a payment \n" + "  4 - Back \n");
			menu = scan.nextInt();
			switch (menu) {
			case 1:
				//createAccount();
				break;
			case 2:
				//inspectAccount();
				break;
			case 3:
				//paymentMethod();
				break;
			case 4:
				accountMenu.accountMenu();
				break;
			default:
				System.out.println("Error choice not on menu. \n");
			}
		}
	}
}
