package repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.util.Date;

import entity.Person;
import entity.User;


public class CreatePerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				//create session factory
						SessionFactory factory = new Configuration()
												.configure("hibernate.cfg.xml")
												.addAnnotatedClass(User.class)
												.addAnnotatedClass(Person.class)
												.buildSessionFactory();
						
												
						//create session
						Session session = factory.getCurrentSession();
						
						try {

							System.out.println("Creating new user object...");
	
							session.beginTransaction();
							
							User user = new User();
							Date date= new Date();
							user.setUsername("ana");
							user.setPassword("1234");
							user.setCreatedTime(date);

							Person person = new Person();
							person.setAddress("Suceava");
							person.setFirstName("Adina");
							person.setFirstName("Caciur");


							user.setPerson(person);
							person.setUser(user);
							
	
							System.out.println("Saving the user..");
							session.save(user);
							session.save(person);
							
							//commit transaction
							session.getTransaction().commit();
							
							System.out.println("Done!!!Uraaaa");
						}
						finally {
							factory.close();
						}

	}

}

