package repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.util.Date;

import entity.User;



public class CreateUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//create session factory
				SessionFactory factory = new Configuration()
										.configure("hibernate.cfg.xml")
										.addAnnotatedClass(User.class)
										.buildSessionFactory();
				
										
				//create session
				Session session = factory.getCurrentSession();
				
				try {
					
					//create a student object
					Date date = new Date();
					System.out.println("Creating new user object...");
					User user = new User("Adina", "test",date);
					
					//start a transaction
					session.beginTransaction();
					
					//save the student object
					System.out.println("Saving the user..");
					session.save(user);
					
					//commit transaction
					session.getTransaction().commit();
					
					System.out.println("Done!!!Uraaaa");
				}
				finally {
					factory.close();
				}

	}

}
