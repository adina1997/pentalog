package repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import entity.Person;
import entity.User;
import entity.Account;


public class CreateAccount {

	public static void main(String[] args) {
	
		// create session factory
				SessionFactory factory = new Configuration()
										.configure("hibernate.cfg.xml")
										.addAnnotatedClass(User.class)
										.addAnnotatedClass(Person.class)
										.addAnnotatedClass(Account.class)
										.buildSessionFactory();
				
				// create session
				Session session = factory.getCurrentSession();
				
				try {			
					
					// start a transaction
					session.beginTransaction();
				
					List<User> theUsers = session.createQuery("from User").list();
					
					displayUsers(theUsers);
				
					theUsers = session.createQuery("from User s where s.id=1").list();
			
	
					System.out.println("\n\nUser with id 1 is: ");
					displayUsers(theUsers);

					session.getTransaction().commit();
					
					System.out.println("Done!");
				}
				catch (HibernateException e) {
                    e.printStackTrace(System.err);
 
				}
			}

			private static void displayUsers(List<User> theUsers) {
				for (User tempUser : theUsers) {
					System.out.println(tempUser);
				}

	}

}

